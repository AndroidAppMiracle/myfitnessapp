package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/8/2017.
 */

public class TranerDetail {
    private String lastName;

    private String[] reviews;

    private String[] trainees;

    private String accessToken;

    private String countryCode;

    private String[] exercise;

    private String[] specializations;

    private String deleted;

    private String[] routines;

    private String phoneNo;

    private String[] questionnaire;

    private String[] certifications;

    private String _id;

    private String email;

    private String[] awards;

    private String[] social;

    private String admin;

    private String role;

    private String firstName;

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String[] getReviews ()
    {
        return reviews;
    }

    public void setReviews (String[] reviews)
    {
        this.reviews = reviews;
    }

    public String[] getTrainees ()
    {
        return trainees;
    }

    public void setTrainees (String[] trainees)
    {
        this.trainees = trainees;
    }

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String[] getExercise ()
    {
        return exercise;
    }

    public void setExercise (String[] exercise)
    {
        this.exercise = exercise;
    }

    public String[] getSpecializations ()
    {
        return specializations;
    }

    public void setSpecializations (String[] specializations)
    {
        this.specializations = specializations;
    }

    public String getDeleted ()
    {
        return deleted;
    }

    public void setDeleted (String deleted)
    {
        this.deleted = deleted;
    }

    public String[] getRoutines ()
    {
        return routines;
    }

    public void setRoutines (String[] routines)
    {
        this.routines = routines;
    }

    public String getPhoneNo ()
    {
        return phoneNo;
    }

    public void setPhoneNo (String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String[] getQuestionnaire ()
    {
        return questionnaire;
    }

    public void setQuestionnaire (String[] questionnaire)
    {
        this.questionnaire = questionnaire;
    }

    public String[] getCertifications ()
    {
        return certifications;
    }

    public void setCertifications (String[] certifications)
    {
        this.certifications = certifications;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String[] getAwards ()
    {
        return awards;
    }

    public void setAwards (String[] awards)
    {
        this.awards = awards;
    }

    public String[] getSocial ()
    {
        return social;
    }

    public void setSocial (String[] social)
    {
        this.social = social;
    }

    public String getAdmin ()
    {
        return admin;
    }

    public void setAdmin (String admin)
    {
        this.admin = admin;
    }

    public String getRole ()
    {
        return role;
    }

    public void setRole (String role)
    {
        this.role = role;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lastName = "+lastName+", reviews = "+reviews+", trainees = "+trainees+", accessToken = "+accessToken+", countryCode = "+countryCode+", exercise = "+exercise+", specializations = "+specializations+", deleted = "+deleted+", routines = "+routines+", phoneNo = "+phoneNo+", questionnaire = "+questionnaire+", certifications = "+certifications+", _id = "+_id+", email = "+email+", awards = "+awards+", social = "+social+", admin = "+admin+", role = "+role+", firstName = "+firstName+"]";
    }
}
