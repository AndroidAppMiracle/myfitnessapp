package com.myfitnessapp.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 8/1/2017.
 */

public class DemoDataModal implements Parcelable {

    private int id;
    private String name;
    private String image;
    private String songUrl;
    private String albumArt;
    private String paymentType;
    private boolean isDownloaded;

    public DemoDataModal(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        songUrl = in.readString();
        albumArt = in.readString();
        paymentType = in.readString();
        isDownloaded = in.readByte() != 0;
    }

    public static final Creator<DemoDataModal> CREATOR = new Creator<DemoDataModal>() {
        @Override
        public DemoDataModal createFromParcel(Parcel in) {
            return new DemoDataModal(in);
        }

        @Override
        public DemoDataModal[] newArray(int size) {
            return new DemoDataModal[size];
        }
    };

    public DemoDataModal() {
        id = 0;
        name = "";
        image = "";
        songUrl = "";
        albumArt = "";
        paymentType = "";
        isDownloaded = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSongUrl() {
        return songUrl;
    }

    public void setSongUrl(String songUrl) {
        this.songUrl = songUrl;
    }

    public String getAlbumArt() {
        return albumArt;
    }

    public void setAlbumArt(String albumArt) {
        this.albumArt = albumArt;
    }


    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public boolean getIsDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(boolean isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(songUrl);
        dest.writeString(albumArt);
        dest.writeString(paymentType);
        dest.writeByte((byte) (isDownloaded ? 1 : 0));
    }
}
