package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/8/2017.
 */

public class TrainerRegistrationModel {
    private String message;

    private String statusCode;

    private TrainerRegInderModel data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public TrainerRegInderModel getData ()
    {
        return data;
    }

    public void setData (TrainerRegInderModel data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", statusCode = "+statusCode+", data = "+data+"]";
    }
}
