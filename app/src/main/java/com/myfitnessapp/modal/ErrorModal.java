package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/8/2017.
 */

public class ErrorModal {
    private String message;

    private String statusCode;

    private String error;

    private String responseType;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    public String getResponseType ()
    {
        return responseType;
    }

    public void setResponseType (String responseType)
    {
        this.responseType = responseType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", statusCode = "+statusCode+", error = "+error+", responseType = "+responseType+"]";
    }
}
