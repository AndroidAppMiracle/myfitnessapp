package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 8/8/2017.
 */

public class DemoGraphModal {

    private int x, y;


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
