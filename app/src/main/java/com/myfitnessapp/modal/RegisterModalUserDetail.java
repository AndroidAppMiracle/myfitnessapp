package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/1/2017.
 */

public class RegisterModalUserDetail {
    private String lastName;

    private String accessToken;

    private String countryCode;

    private String __v;

    private String password;

    private String deleted;

    private String phoneNo;

    private String _id;

    private String email;

    private Social[] social;

    private String admin;

    private String role;

    private String firstName;

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getDeleted ()
    {
        return deleted;
    }

    public void setDeleted (String deleted)
    {
        this.deleted = deleted;
    }

    public String getPhoneNo ()
    {
        return phoneNo;
    }

    public void setPhoneNo (String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public Social[] getSocial ()
    {
        return social;
    }

    public void setSocial (Social[] social)
    {
        this.social = social;
    }

    public String getAdmin ()
    {
        return admin;
    }

    public void setAdmin (String admin)
    {
        this.admin = admin;
    }

    public String getRole ()
    {
        return role;
    }

    public void setRole (String role)
    {
        this.role = role;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lastName = "+lastName+", accessToken = "+accessToken+", countryCode = "+countryCode+", __v = "+__v+", password = "+password+", deleted = "+deleted+", phoneNo = "+phoneNo+", _id = "+_id+", email = "+email+", social = "+social+", admin = "+admin+", role = "+role+", firstName = "+firstName+"]";
    }
}
