package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/8/2017.
 */

public class TrainerRegInderModel {
    private TranerDetail tranerDetail;

    public TranerDetail getTranerDetail()
    {
        return tranerDetail;
    }

    public void setTranerDetail(TranerDetail tranerDetail)
    {
        this.tranerDetail = tranerDetail;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tranerDetail = "+ tranerDetail +"]";
    }
}
