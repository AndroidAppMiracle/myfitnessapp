package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/8/2017.
 */

public class Social {
    private String socialId;

    private String socialMode;

    public String getSocialId ()
    {
        return socialId;
    }

    public void setSocialId (String socialId)
    {
        this.socialId = socialId;
    }

    public String getSocialMode ()
    {
        return socialMode;
    }

    public void setSocialMode (String socialMode)
    {
        this.socialMode = socialMode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [socialId = "+socialId+", socialMode = "+socialMode+"]";
    }
}
