package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/1/2017.
 */

public class RegisterModalData {
    private String accessToken;

    private RegisterModalUserDetail userDetails;

    public String getAccessToken ()
    {
        return accessToken;
    }

    public void setAccessToken (String accessToken)
    {
        this.accessToken = accessToken;
    }

    public RegisterModalUserDetail getRegisterModalUserDetail ()
    {
        return userDetails;
    }

    public void setRegisterModalUserDetail (RegisterModalUserDetail userDetails)
    {
        this.userDetails = userDetails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [accessToken = "+accessToken+", userDetails = "+userDetails+"]";
    }
}
