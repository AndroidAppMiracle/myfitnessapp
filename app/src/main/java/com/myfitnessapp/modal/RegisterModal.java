package com.myfitnessapp.modal;

/**
 * Created by satoti.garg on 11/1/2017.
 */

public class   RegisterModal {
    private String message;

    private String statusCode;

    private RegisterModalData data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public RegisterModalData getRegisterModalData ()
    {
        return data;
    }

    public void setRegisterModalData (RegisterModalData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", statusCode = "+statusCode+", data = "+data+"]";
    }
}
