package com.myfitnessapp.custome_controls;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by satoti.garg on 10/6/2017.
 */

public class CustomeEditTextRegular extends EditText {

    public CustomeEditTextRegular(Context context)
    {
        super(context); setFont();
    }

    public CustomeEditTextRegular(Context context, AttributeSet set)
    {
        super(context,set); setFont();
    }

    public CustomeEditTextRegular(Context context, AttributeSet set, int defaultStyle)
    {
        super(context,set,defaultStyle); setFont();
    }

    private void setFont() {

        Typeface typeface=Typeface.createFromAsset(getContext().getAssets(),"Gotham-Medium.otf");
        setTypeface(typeface);
        //function used to set font

    }
}
