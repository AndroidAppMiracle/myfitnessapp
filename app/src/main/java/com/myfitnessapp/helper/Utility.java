package com.myfitnessapp.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.myfitnessapp.R;

import java.util.Locale;


/**
 * Created by Kshitiz Bali on 12/9/2016.
 */

public class Utility extends AppCompatActivity {
    public String query;
    private AlertDialog loadingDialog;
    private SharedPreferences prefrence;
    // private Locale myLocale;


  /*  @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("+++++++++","On Create Utility..");
        prefrence = new ObscuredSharedPreferences(Utility.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
    }*/

    /**
     * Hides the soft keyboard
     */
  /*  public static void hideSoftKeyboard(Context context) {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
*/
    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null && inputManager != null) {
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                inputManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

/*    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }*/


    //Convert dp to pixel:

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = Utility.this.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    //Convert pixel to dp:

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }


    public static void includeProgressDialog(Context context, /*String Message,*/ boolean showFlag) {

        try {
            if (context != null) {
                ProgressDialog dlg = new ProgressDialog(context);
                if (showFlag) {
                    dlg.setMessage("Loading...");
                    dlg.setCancelable(false);
                    dlg.show();
                } else {
                    if (dlg.isShowing()) {
                        dlg.dismiss();
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean checkInternetConnection(Context cnt) {
        try {
            ConnectivityManager cm = (ConnectivityManager) cnt
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiNetwork = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifiNetwork != null && wifiNetwork.isConnected()) {
                return true;
            }

            NetworkInfo mobileNetwork = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mobileNetwork != null && mobileNetwork.isConnected()) {
                return true;
            }

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null && activeNetwork.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void showLoading() {
        try {
            if (loadingDialog == null) {
                LayoutInflater factory = LayoutInflater.from(this);
                final View deleteDialogView = factory.inflate(R.layout.layout_dialog, null);
                loadingDialog = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).create();
                loadingDialog.setView(deleteDialogView);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.setCancelable(false);
            }
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissDialog() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoading() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showAlertDialog(Context context) {

        try {
            // final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);


            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
            dialogBuilder.setTitle("Connection Problem");
            dialogBuilder.setMessage("Please check your internet connection.");
            dialogBuilder.setCancelable(true);
           /* dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = false;
                }
            });*/
            dialogBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //onBackExitFlag = true;
                }
            });
           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static String headerUserID(Context context) {
        SharedPreferences prefrence = new ObscuredSharedPreferences(context, context.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        return Session.get_userId(prefrence);
    }*/

    /*@Override
    protected void attachBaseContext(Context newBase) {

        prefrence = new ObscuredSharedPreferences(Utility.this, getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));

        String languageChoose = "en";
       *//* if (Session.getAppLanguage(prefrence).equalsIgnoreCase("1")) {
            languageChoose = "en";
        } else if (Session.getAppLanguage(prefrence).equalsIgnoreCase("2")) {
            languageChoose = "in";
        }*//*
        Locale newLocale = new Locale(languageChoose);
        // .. create or get your new Locale object here.
        //newLocale = setDefaultLocal()
        Context context = ContextWrapper.wrap(newBase, newLocale);
        super.attachBaseContext(context);
    }*/


    @Override
    protected void attachBaseContext(Context newBase) {

        /*SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        languageChoosen = pref.getString("app_language", "1");*/
        String languageChoosen = "";
        Log.d("+++++++++", "before.");
       // prefrence = new ObscuredSharedPreferences(newBase, newBase.getSharedPreferences(Session.PREFERENCES, Context.MODE_PRIVATE));
        Log.d("+++++++++", "After..");
      /*  if (Session.getAppLanguage(prefrence).equalsIgnoreCase("1")) {
            languageChoosen = "en";
        } else {
            languageChoosen = "in";
        }
        Locale locale = new Locale(languageChoosen);*/
        //super.attachBaseContext(ContextWrapper.wrap(newBase, locale));
    }


    /*    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        // saveLocale(lang);
        Session.setAppLanguage(getSharedPreferences(Session.PREFERENCES, MODE_PRIVATE), lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }*/
}
