package com.myfitnessapp.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.myfitnessapp.R;
import com.myfitnessapp.helper.Constants;
import com.myfitnessapp.helper.ObscuredSharedPreferences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class MyFitnessBaseActivity extends AppCompatActivity {

    ObscuredSharedPreferences prefs;
    private Snackbar snackbar;
    private AlertDialog loadingDialog;


    private SharedPreferences mPrefs, tokenPrefs;

    public SharedPreferences getmPrefs() {
        return mPrefs;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = ObscuredSharedPreferences.getPrefs(this, getString(R.string.app_name), Context.MODE_PRIVATE);
        mPrefs = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        tokenPrefs = getSharedPreferences(Constants.DEVICE_ID, MODE_PRIVATE);
    }
    public SharedPreferences gettokenPrefs() {
        return tokenPrefs;
    }



    public static String getDeviceID(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public void setFcm(String fcmToken)
    {
      Constants.FCM_TOKEN=fcmToken;
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    /**
     * For hiding the loading
     */
    public void hideLoading() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * For Hiding the keyboard
     */
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    /**
     * For Displaying the loading when a service is hit
     */
    public void showLoading() {
        try {
            if (loadingDialog == null) {
                LayoutInflater factory = LayoutInflater.from(this);
                final View deleteDialogView = factory.inflate(R.layout.layout_dialog, null);
                loadingDialog = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).create();
                loadingDialog.setView(deleteDialogView);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.setCancelable(false);
            }
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissDialog() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFirebaseToken() {

        if (gettokenPrefs().getString(Constants.FCM_TOKEN, "").equalsIgnoreCase("")) {
            getmPrefs().edit().putString(Constants.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken()).apply();
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        } else {
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        }

    }


    /**
     * function for fetching user ID
     *
     * @return user ID
     */
    public String getUserFcmToken() {
        return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
    }
    /**
     * For Displaying the snackBar
     *
     * @param msg message to display in the snackBar.
     */
    public void showSnack(String msg) {
        try {
            snackbar = Snackbar.make(getCurrentFocus(), msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    public void showSnack(String msg, View view) {
        try {
            snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Function to display Toast
     *
     * @param message  Message of the Toast
     * @param duration Duration of the Toast 0 for Short and 1 for Long Toast
     */
    public void showToast(String message, int duration) {
        if (message != null && message.length() > 0) {
            if (duration == 0) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * For Displaying Output in LogCat
     *
     * @param message Message to display
     */
    public void showOutput(String message) {
        System.out.println("------------------------");
        System.out.println(message);
        System.out.println("------------------------");
    }


    /**
     * For Displaying the Log Output in Logcat
     *
     * @param tag     Tag of the Log
     * @param message Message of the Log
     */
    public void showLog(String tag, String message) {
        System.out.println("--------------------");
        Log.d(tag, message);
        System.out.println("--------------------");
    }

    /**
     * function to check if internet connection is active or not.
     *
     * @return true if connected to internet else false
     */
    public boolean isConnectedToInternet() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null;
    }


    private static String userID = "user_id";
    private static String userIdDefValue = "";

    public void setUserID(String userIDValue) {
        prefs.edit().putString(userID, userIDValue).apply();
    }

    public String getUserID() {
        return prefs.getString(userID, userIdDefValue);
    }

    private static String emailId = "email_id";
    private static String emailIdDefValue = "";

    public void setEmailID(String emailIdValue) {
        prefs.edit().putString(emailId, emailId).apply();
    }

    public String getEmailID() {
        return prefs.getString(emailId, emailIdDefValue);
    }


    private static String userType = "user_type";
    private static String userTypeDefValue = "";


    public void setUserType(String userTypeValue) {
        prefs.edit().putString(userType, userTypeValue).apply();
    }

    public String getUserType() {
        return prefs.getString(userType, userTypeDefValue);
    }


    private static String firstName = "first_name";
    private static String firstNameDefValue = "";


    public void setFirstName(String firstNameValue) {
        prefs.edit().putString(firstName, firstNameValue).apply();
    }

    public String getFirstName() {
        return prefs.getString(firstName, firstNameDefValue);
    }

    private static String lastName = "last_name";
    private static String lastNameDefValue = "";


    public void setLastName(String lastNameValue) {
        prefs.edit().putString(lastName, lastNameValue).apply();
    }

    public String getLastName() {
        return prefs.getString(lastName, lastNameDefValue);
    }

    private static String phoneNumber = "phone_number";
    private static String phoneNumberDefValue = "";


    public void setPhoneNumber(String phoneNumberValue) {
        prefs.edit().putString(phoneNumber, phoneNumberValue).apply();
    }

    public String getPhoneNumber() {
        return prefs.getString(phoneNumber, phoneNumberDefValue);
    }


    private static String dob = "dob";
    private static String dobDefValue = "";


    public void setDOB(String dobValue) {
        prefs.edit().putString(dob, dobValue).apply();
    }

    public String getDOB() {
        return prefs.getString(dob, dobDefValue);
    }


    private static String gender = "gender";
    private static String genderDefValue = "";


    public void setGender(String genderValue) {
        prefs.edit().putString(gender, genderValue).apply();
    }

    public String getGender() {
        return prefs.getString(gender, genderDefValue);
    }

    private static String city = "city";
    private static String cityDefValue = "";


    public void setCity(String cityValue) {
        prefs.edit().putString(city, cityValue).apply();
    }

    public String getCity() {
        return prefs.getString(city, cityDefValue);
    }

    private static String loggedIn = "logged_in";
    private static boolean loggedInDefValue = false;

    public void setIsLoggedIn(boolean loggedInValue) {
        prefs.edit().putBoolean(loggedIn, loggedInValue).apply();
    }

    public boolean getIsLoggedIn() {
        return prefs.getBoolean(loggedIn, loggedInDefValue);
    }


    private static String firebaseToken = "firebase_token";
    private static String firebaseTokenDefValue = "";


    public void setFirebaseToken(String firebaseTokenValue) {
        prefs.edit().putString(firebaseToken, firebaseTokenValue).apply();
    }

    /*public String getFirebaseToken() {
        return prefs.getString(firebaseToken, firebaseTokenDefValue);
    }*/


    private static String accessToken = "accessToken";
    private static String accessTokenValue = "";


    public void setAccessToken(String accessToken) {
        prefs.edit().putString(this.accessToken, accessToken).apply();
    }

    public String getAccessToken() {
        return prefs.getString(accessToken, accessTokenValue);
    }

    private static String userRole = "user_role";
    private static String userRoleValue = "";


    public void setUserRole(String userRole) {
        prefs.edit().putString(this.userRole, userRole).apply();
    }

    public String getUserRole() {
        return prefs.getString(userRole, userRoleValue);
    }


    public void setUserLoggedIn(Boolean logged) {
        getmPrefs().edit().putBoolean(Constants.SHARED_PREF_NAME, true).apply();
    }

    public Boolean getUserLoggedIn() {
        return getmPrefs().getBoolean(Constants.SHARED_PREF_NAME, false);
    }
    public void clearPreferences() {
        mPrefs.edit().clear().apply();
        mPrefs.edit().clear().commit();
    }
}
