package com.myfitnessapp.helper;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class Constants {

    public static String SECURED_PREFERENCES = "my_fitness_app_preference";
    public static final String FIRST_NAMENOT_BLANK = "First name should not be blank.";
    public static final String FIRST_NAME_START = "Name should not starts with numeric character.";
    public static final String EMAIL_START = "Email should not starts with numeric character.";
    public static final String LAST_NAMENOT_BLANK = "Last name should not be blank.";
    public static final String COUNTRY_CODE_NOT_BLANK = "Country code should not be blank.";
    public static final String COUNTRY_CODE_LENGTH = "Country code should be Length greater than 3.";
    public static final String PHONE_NUMBER_NOT_BLANK = "Phone Number should not be blank.";
    public static final String FAT_PERCENTAGE = "Invalide Fat quantity";
    public static final String PHONE_NUMBER_LENGTH = "Phone number Length greater than 10.";
    public static final String EMAIL_NOT_BLANK = "Email Id should not be blank.";
    public static final String EMAIL_NOT_VALID = "Enter Valid Email Id.";
    public static final String PASSWORD_NOT_BLANK = "Password should not be blank.";
    public static final String PASSWORD_LENGTH = "Password Length should be equals to or greater than 6.";
    public static final String INTERNET_CONNECTION = "Please check your internet connection.";
    public static final String SHARED_PREF_USER_SESSION_ID = "MyFitnessUserSessionID";
    public static String FCM_TOKEN = "token";
    public static final String DEVICE_ID = "Device_ID";
    public static final String SHARED_PREF_NAME = "MyFitnessShared";

}
