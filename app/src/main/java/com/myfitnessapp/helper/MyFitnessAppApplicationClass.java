package com.myfitnessapp.helper;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class MyFitnessAppApplicationClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        LeakCanary.install(this);
        // Normal app init code...
    }
}
