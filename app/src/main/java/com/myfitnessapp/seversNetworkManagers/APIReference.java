package com.myfitnessapp.seversNetworkManagers;

import com.myfitnessapp.modal.RegisterModal;
import com.myfitnessapp.modal.StatusMessageDataResponse;
import com.myfitnessapp.modal.TrainerRegistrationModel;

import com.google.gson.*;


import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


/**
 * Created by satoti.garg on 11/1/2017.
 */

public interface APIReference {
    @Multipart
    @POST("api/trainee/createTrainee")
    Call<RegisterModal> traineeSignup(/*@Part("firstName") RequestBody firstName, @Part("lastName") RequestBody lastName,
                                      @Part("countryCode") RequestBody countryCode, @Part("phoneNo") RequestBody phoneNo,
                                      @Part("email") RequestBody email, @Part("password") RequestBody password,
                                      @Part("dateOfBirth") RequestBody dateOfBirth, @Part("gender") RequestBody gender,
                                      @Part("socialId") RequestBody socialId, @Part("socialMode") RequestBody socialMode,
                                      @Part("deviceType") RequestBody deviceType, @Part("deviceToken") RequestBody deviceToken,
                                      @Part("appVersion") RequestBody appVersion, @Part("fat") RequestBody fat,
                                      @Part("weight") RequestBody weight, @Part("recentGym") RequestBody recentGym,
                                      @Part("city") RequestBody city, @Part("trainer") RequestBody trainer*/@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part profile_pic
    );
/*@PartMap Map<String, String> params*/
    @Multipart
    @POST("api/user/login")
    Call<JsonObject> login(@Part("email") RequestBody emailId, @Part("password") RequestBody socialMode,
                           @Part("deviceType") RequestBody deviceType, @Part("deviceToken") RequestBody deviceToken
                              );


    @Multipart
    @POST("api/trainee/createTrainee")
    Call<RegisterModal> socialSignIn(@PartMap Map<String, RequestBody> params);
    @Multipart
    @POST("api/trainee/createTrainee")
    Call<TrainerRegistrationModel> trainerSocialSignIn(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("api/user/forgotPassword")
    Call<StatusMessageDataResponse> resetPassword(@Part("email") RequestBody emailId);


    @Multipart
    @POST("api/trainer/trainerRegisteration")
    Call<TrainerRegistrationModel> trainerResgisteration(@PartMap Map<String, RequestBody> params, @Part MultipartBody.Part profile_pic);
    @Multipart
    @POST("api/trainee/login")
    Call<RegisterModal> forgetPassword(@Part("email") RequestBody socialId, @Part("password") RequestBody socialMode,
                              @Part("deviceType") RequestBody deviceType, @Part("deviceToken") RequestBody deviceToken
    );



}
