package com.myfitnessapp.seversNetworkManagers;

import retrofit2.Response;

/**
 * Created by satoti.garg on 11/1/2017.
 */

public interface APIServerResponse {


    int REGISTER = 1;
    int LOGIN = 2;
    int FORGET_PASSWORD = 3;
    int TRAINEE_REGISTER = 4;
    void onSuccess(int tag, Response response);

    void onError(int tag, Throwable throwable);
}
