package com.myfitnessapp.seversNetworkManagers;

import android.content.SharedPreferences;

import com.myfitnessapp.modal.RegisterModal;
import com.myfitnessapp.modal.StatusMessageDataResponse;
import com.myfitnessapp.modal.TrainerRegistrationModel;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by satoti.garg on 11/1/2017.
 */

public class ServerAPI {
    private static ServerAPI ourInstance = new ServerAPI();
    /*private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();*/
    private Retrofit mRetrofit;
    private APIReference apiReference;
    private SharedPreferences sharedPrefs;

    public ServerAPI() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addNetworkInterceptor(logging);
        httpClient.addInterceptor(new AddHeaderInterceptor());
        // <-- this is the important line!
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl("http://172.16.0.2:8030/")
                    .addConverterFactory(GsonConverterFactory.create()).client(httpClient.build())
                    .build();
            apiReference = mRetrofit.create(APIReference.class);
        }

    }


    public class AddHeaderInterceptor implements Interceptor {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {

            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("Content-Type", "application/json");
            return chain.proceed(builder.build());
        }
    }


    public static ServerAPI getInstance() {
        return ourInstance;
    }


    //Trainees
    public void registerTrainee(final int tag, String first_name, String last_name, String email, String pass,
                                String countryCode, String socialId, String socialMode,
                                String FCMToken, String device_type, String gender, String date_of_birth,
                                String phone_number, String appVersion,
                                String fatSt, String weightSt, String recetnGym, String city, String trainer,
                                String profile_pic, final APIServerResponse apiServerResponse) {

        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profile_pic));
        MultipartBody.Part body = MultipartBody.Part.createFormData("profile_image", "user_profile.jpg", requestBody);

        RequestBody firstNameReq = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody passReq = RequestBody.create(MediaType.parse("text/plain"), pass);
        RequestBody countryCodeReq = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody socialReq = RequestBody.create(MediaType.parse("text/plain"), socialId);
        RequestBody socialModeReq = RequestBody.create(MediaType.parse("text/plain"), socialMode);
        RequestBody FCMTokenReq = RequestBody.create(MediaType.parse("text/plain"), FCMToken);
        RequestBody device_typeReq = RequestBody.create(MediaType.parse("text/plain"), device_type);
        RequestBody genderReq = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody date_of_birthReq = RequestBody.create(MediaType.parse("text/plain"), date_of_birth);
        RequestBody phone_numberReq = RequestBody.create(MediaType.parse("text/plain"), phone_number);
        RequestBody appVersionReq = RequestBody.create(MediaType.parse("text/plain"), appVersion);
        RequestBody fatStReq = RequestBody.create(MediaType.parse("text/plain"), fatSt);
        RequestBody weightStReq = RequestBody.create(MediaType.parse("text/plain"), weightSt);

        RequestBody recetnGymReq = RequestBody.create(MediaType.parse("text/plain"), recetnGym);
        RequestBody cityReq = RequestBody.create(MediaType.parse("text/plain"), city);
        RequestBody trainerReq = RequestBody.create(MediaType.parse("text/plain"), "");

        Map<String, RequestBody> params = new HashMap<>();
        params.put("firstName", firstNameReq);
        params.put("lastName", lastNameReq);
        params.put("countryCode", countryCodeReq);
        params.put("phoneNo", phone_numberReq);
        params.put("email", emailReq);
        params.put("password", passReq);
        params.put("dateOfBirth", date_of_birthReq);
        params.put("gender", genderReq);
        params.put("deviceType", device_typeReq);
        params.put("deviceToken", FCMTokenReq);
        params.put("appVersion", appVersionReq);
        params.put("fat", fatStReq);
        params.put("weight", weightStReq);
        params.put("recentGym", recetnGymReq);
        params.put("city", cityReq);


        Call<RegisterModal> call = apiReference.traineeSignup(params, body);
        call.enqueue(new Callback<RegisterModal>() {
            @Override
            public void onResponse(Call<RegisterModal> call, Response<RegisterModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RegisterModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void traineeSocialLogin(final int tag, String first_name, String last_name, String email, String pass,
                                   String countryCode, String socialId, String socialMode,
                                   String FCMToken, String device_type, String gender, String date_of_birth,
                                   String phone_number, String appVersion,
                                   String fatSt, String weightSt, String recetnGym, String city, String trainer,
                                   String profile_pic, final APIServerResponse apiServerResponse) {
        RequestBody firstNameReq = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody countryCodeReq = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody socialReq = RequestBody.create(MediaType.parse("text/plain"), socialId);
        RequestBody socialModeReq = RequestBody.create(MediaType.parse("text/plain"), socialMode);
        RequestBody FCMTokenReq = RequestBody.create(MediaType.parse("text/plain"), FCMToken);
        RequestBody device_typeReq = RequestBody.create(MediaType.parse("text/plain"), device_type);
        RequestBody genderReq = RequestBody.create(MediaType.parse("text/plain"), gender);

        RequestBody phone_numberReq = RequestBody.create(MediaType.parse("text/plain"), phone_number);
        RequestBody appVersionReq = RequestBody.create(MediaType.parse("text/plain"), appVersion);

        Map<String, RequestBody> params = new HashMap<>();
        params.put("firstName", firstNameReq);
        params.put("lastName", lastNameReq);
        params.put("countryCode", countryCodeReq);
        params.put("phoneNo", phone_numberReq);
        params.put("email", emailReq);
        params.put("gender", genderReq);
        params.put("socialId", socialReq);
        params.put("socialMode", socialModeReq);
        params.put("deviceType", device_typeReq);
        params.put("deviceToken", FCMTokenReq);
        params.put("appVersion", appVersionReq);
        // params.put("profile_image",profile);


        Call<RegisterModal> call = apiReference.socialSignIn(params);
        call.enqueue(new Callback<RegisterModal>() {
            @Override
            public void onResponse(Call<RegisterModal> call, Response<RegisterModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RegisterModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void login(final int tag, String email, String password, String deviceType, String deviceToken, final APIServerResponse apiServerResponse) {
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody passwordReq = RequestBody.create(MediaType.parse("text/plain"), password);
        RequestBody deviceTypereq = RequestBody.create(MediaType.parse("text/plain"), deviceType);
        RequestBody deviceTokenReq = RequestBody.create(MediaType.parse("text/plain"), deviceToken);

        Call call = apiReference.login(emailReg, passwordReq, deviceTypereq, deviceTokenReq);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    //Trainer
    public void registerTrainer(final int tag, String first_name, String last_name, String email, String pass,
                                String countryCode,
                                String FCMToken, String device_type, String gender, String date_of_birth,
                                String phone_number, String appVersion,
                                String specializations, String certifications, String description,
                                String profile_pic, final APIServerResponse apiServerResponse) {

        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profile_pic));
        MultipartBody.Part body = MultipartBody.Part.createFormData("profile_image", "user_profile.jpg", requestBody);

        RequestBody firstNameReq = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody passReq = RequestBody.create(MediaType.parse("text/plain"), pass);
        RequestBody countryCodeReq = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody FCMTokenReq = RequestBody.create(MediaType.parse("text/plain"), FCMToken);
        RequestBody device_typeReq = RequestBody.create(MediaType.parse("text/plain"), device_type);
        RequestBody genderReq = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody date_of_birthReq = RequestBody.create(MediaType.parse("text/plain"), date_of_birth);
        RequestBody phone_numberReq = RequestBody.create(MediaType.parse("text/plain"), phone_number);
        RequestBody appVersionReq = RequestBody.create(MediaType.parse("text/plain"), appVersion);
        RequestBody specializationsStReq = RequestBody.create(MediaType.parse("text/plain"), specializations);
        RequestBody certificationsReq = RequestBody.create(MediaType.parse("text/plain"), certifications);
        RequestBody descriptionReq = RequestBody.create(MediaType.parse("text/plain"), description);


        Map<String, RequestBody> params = new HashMap<>();
        params.put("firstName", firstNameReq);
        params.put("lastName", lastNameReq);
        params.put("countryCode", countryCodeReq);
        params.put("phoneNo", phone_numberReq);
        params.put("email", emailReq);
        params.put("password", passReq);
        params.put("dateOfBirth", date_of_birthReq);
        params.put("gender", genderReq);
        params.put("deviceType", device_typeReq);
        params.put("deviceToken", FCMTokenReq);
        params.put("appVersion", appVersionReq);
        params.put("specializations", specializationsStReq);
        params.put("certifications", certificationsReq);
        params.put("description", descriptionReq);

        Call<TrainerRegistrationModel> call = apiReference.trainerResgisteration(params, body);
        call.enqueue(new Callback<TrainerRegistrationModel>() {
            @Override
            public void onResponse(Call<TrainerRegistrationModel> call, Response<TrainerRegistrationModel> response) {

                    apiServerResponse.onSuccess(tag, response);

            }

            @Override
            public void onFailure(Call<TrainerRegistrationModel> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }

    public void trainerSocialLogin(final int tag, String first_name, String last_name, String email, String pass,
                                String countryCode, String socialId, String socialMode,
                                String FCMToken, String device_type, String gender, String date_of_birth,
                                String phone_number, String appVersion,
                                String specializations, String certifications, String description,
                                String profile_pic, final APIServerResponse apiServerResponse) {

        RequestBody firstNameReq = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody lastNameReq = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody countryCodeReq = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody FCMTokenReq = RequestBody.create(MediaType.parse("text/plain"), FCMToken);
        RequestBody socialReq = RequestBody.create(MediaType.parse("text/plain"), socialId);
        RequestBody socialModeReq = RequestBody.create(MediaType.parse("text/plain"), socialMode);
        RequestBody device_typeReq = RequestBody.create(MediaType.parse("text/plain"), device_type);
        RequestBody genderReq = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody phone_numberReq = RequestBody.create(MediaType.parse("text/plain"), phone_number);
        RequestBody appVersionReq = RequestBody.create(MediaType.parse("text/plain"), appVersion);

        Map<String, RequestBody> params = new HashMap<>();
        params.put("firstName", firstNameReq);
        params.put("lastName", lastNameReq);
        params.put("countryCode", countryCodeReq);
        params.put("phoneNo", phone_numberReq);
        params.put("email", emailReq);
        params.put("gender", genderReq);
        params.put("socialId", socialReq);
        params.put("socialMode", socialModeReq);
        params.put("deviceType", device_typeReq);
        params.put("deviceToken", FCMTokenReq);
        params.put("appVersion", appVersionReq);

        Call<TrainerRegistrationModel> call = apiReference.trainerSocialSignIn(params);
        call.enqueue(new Callback<TrainerRegistrationModel>() {
            @Override
            public void onResponse(Call<TrainerRegistrationModel> call, Response<TrainerRegistrationModel> response) {

                apiServerResponse.onSuccess(tag, response);

            }

            @Override
            public void onFailure(Call<TrainerRegistrationModel> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }



    public void resetPassword(final int tag, String email, final APIServerResponse apiServerResponse) {
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        Call<StatusMessageDataResponse> call = apiReference.resetPassword(emailReg);
        call.enqueue(new Callback<StatusMessageDataResponse>() {
            @Override
            public void onResponse(Call<StatusMessageDataResponse> call, Response<StatusMessageDataResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<StatusMessageDataResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void getAllCategories(final int tag, String email, final APIServerResponse apiServerResponse)
    {
        RequestBody emailReg = RequestBody.create(MediaType.parse("text/plain"), email);
        Call<StatusMessageDataResponse> call = apiReference.resetPassword(emailReg);
        call.enqueue(new Callback<StatusMessageDataResponse>() {
            @Override
            public void onResponse(Call<StatusMessageDataResponse> call, Response<StatusMessageDataResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<StatusMessageDataResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
