package com.myfitnessapp.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.myfitnessapp.R;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by satoti.garg on 8/11/2017.
 */

public class SearchTrainerAdapter extends RecyclerView.Adapter<SearchTrainerAdapter.DataViewHolder> {


    private Context mContext;
    private List<DemoDataModal> myFriends = new ArrayList<>();
    /*private boolean checkedStatus = false;*/
    /*private FilterModal modal;*/
    //private ArrayList<DemoDataModal.UserInfoBean.ProfileDetailsBean> taggedFriendsDemo = new ArrayList<>();
    private List<DemoDataModal> myFriendsAll = new ArrayList<>();
    /*private ArrayList<UpdateGroupParcebleModal> groupMembers = new ArrayList<>();*/
    //private ArrayList<UpdateGroupParcebleModal> allFriends = new ArrayList<>();

    public SearchTrainerAdapter(Context mContext, List<DemoDataModal> list) {
        this.myFriends = list;
        this.mContext = mContext;
        this.myFriendsAll.addAll(myFriends);
    }


    @Override
    public SearchTrainerAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_search_trainers, parent, false);
        return new SearchTrainerAdapter.DataViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final SearchTrainerAdapter.DataViewHolder holder, int position) {

        try {

            holder.atvTraineeName.setText(myFriends.get(position).getName());
            holder.atvtraineeDesc.setText(myFriends.get(position).getSongUrl());


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* public ArrayList<FilterModal> unselectList() {
        FilterModal filterModal = new FilterModal();
        filterModal.setFriendsArray(myTaggedFriends);
        return myTaggedFriends;

    }*/

    /*public ArrayList<DemoDataModal.UserInfoBean.ProfileDetailsBean> selectedList() {

        ArrayList<DemoDataModal.UserInfoBean.ProfileDetailsBean> list = new ArrayList<>();
        list = taggedFriendsDemo;
        return list;
    }*/

    @Override
    public int getItemCount() {
        if (myFriends.size() != 0) {
            return myFriends.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atvTraineeName)
        AppCompatTextView atvTraineeName;
        @BindView(R.id.atvtraineeDesc)
        AppCompatTextView atvtraineeDesc;
        @BindView(R.id.civ_Trainee)
        CircleImageView civ_Trainee;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

    }


    public void notifyDataList(List<DemoDataModal> list) {
        Log.d("notifyData ", list.size() + "");
        this.myFriends = list;
        notifyDataSetChanged();
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        myFriends.clear();
        if (charText.length() == 0) {
            myFriends.addAll(myFriendsAll);
        } else {
            for (DemoDataModal wp : myFriendsAll) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    myFriends.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


}
