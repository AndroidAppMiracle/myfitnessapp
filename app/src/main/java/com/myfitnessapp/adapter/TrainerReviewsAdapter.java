package com.myfitnessapp.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.myfitnessapp.R;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class TrainerReviewsAdapter extends RecyclerView.Adapter<TrainerReviewsAdapter.DataViewHolder> {


    private Context mContext;
    private List<DemoDataModal> myGroupsList = new ArrayList<>();
    private SparseBooleanArray selectedItems;

    public TrainerReviewsAdapter(Context mContext, List<DemoDataModal> list) {
        this.myGroupsList = list;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();
    }

    @Override
    public TrainerReviewsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_trainer_reviews, parent, false);
        return new TrainerReviewsAdapter.DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TrainerReviewsAdapter.DataViewHolder holder, final int position) {

        try {
            holder.atvTrainerName.setText(myGroupsList.get(position).getName());
            holder.atvReviewTime.setText(myGroupsList.get(position).getSongUrl());
            holder.atvReviewText.setText(myGroupsList.get(position).getAlbumArt());
            /*myGroupsList.get(position).getImage()*/
            Glide.with(mContext).load(R.drawable.ic_demo_barbell).override(64, 64).crossFade().into(holder.civ_profile_image);
            holder.ratingTrainer.setRating(4);




/*.listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            // image ready, hide progress now
                            holder.mProgress.setVisibility(View.GONE);
                            return false;   // return false if you want Glide to handle everything else.
                        }
                    })
                    *//*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*/
            /*Glide.with(mContext)
                    .load(myGroupsList.get(position).getImage())
                    .centerCrop()
                    .override(64, 64)
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(holder.atv_AlbumCover);*/

            /*Glide.with(mContext).load("").*/

                    /*holder.itemView.setActivated(selectedItems.get(position, false));*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myGroupsList.size() != 0) {
            return myGroupsList.size();
        } else {
            return 0;
        }

    }

    public DemoDataModal getItem(int position) {
        return myGroupsList.get(position);
    }

    public void toggleSelection(int pos) {
        /*currentSelectedIndex = pos;*/
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            /*animationItemsIndex.delete(pos);*/
        } else {
            selectedItems.put(pos, true);
            /*animationItemsIndex.put(pos, true);*/
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        /*reverseAllAnimations = true;*/
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        myGroupsList.remove(position);
        //resetCurrentIndex();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atvTrainerName)
        AppCompatTextView atvTrainerName;

        @BindView(R.id.atvReviewTime)
        AppCompatTextView atvReviewTime;

        @BindView(R.id.atvReviewText)
        AppCompatTextView atvReviewText;


        @BindView(R.id.ratingTrainer)
        RatingBar ratingTrainer;


        @BindView(R.id.civ_profile_image)
        CircleImageView civ_profile_image;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }


}