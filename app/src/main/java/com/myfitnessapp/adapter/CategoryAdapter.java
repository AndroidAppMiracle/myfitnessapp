package com.myfitnessapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.myfitnessapp.R;
import com.myfitnessapp.activity.trainee_module.activity.SearchTrainersActivity;
import com.myfitnessapp.activity.trainer_module.UpdateCertificationsActivity;
import com.myfitnessapp.custome_controls.CustomeTextViewRegular;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 10/6/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.DataViewHolder> {

    Context mContext;

    public CategoryAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.cvCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SearchTrainersActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cat_name)
        CustomeTextViewRegular catName;


        @BindView(R.id.cat_radio)
        /*RadioButton*/ CheckBox catRadio;

        @BindView(R.id.cv_category_name)
        CardView cvCategoryName;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

    }
}
