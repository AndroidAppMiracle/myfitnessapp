package com.myfitnessapp.activity.trainee_module.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.myfitnessapp.R;
import com.myfitnessapp.activity.trainer_module.CertificationsActivity;
import com.myfitnessapp.activity.trainer_module.RoutinesActivity;
import com.myfitnessapp.activity.trainer_module.TraineesActivity;
import com.myfitnessapp.activity.trainer_module.profile.EditProfileActivity;
import com.myfitnessapp.modal.DemoGraphModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by satoti.garg on 8/10/2017.
 */

public class ProfileFragmentTrainee extends Fragment {

    @BindView(R.id.main_toolbar)
    Toolbar main_toolbar;

    @BindView(R.id.iv_EditProfile)
    ImageView iv_EditProfile;

    @BindView(R.id.civ_profile_image)
    CircleImageView civ_profile_image;

    @BindView(R.id.buttonCertifications)
    Button buttonCertifications;

    @BindView(R.id.buttonPreSetRoutine)
    Button buttonPreSetRoutine;

    @BindView(R.id.buttonSpecialization)
    Button buttonSpecialization;


    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_trainee, container, false);
        ButterKnife.bind(this, rootView);


        ((AppCompatActivity) getActivity()).setSupportActionBar(main_toolbar);
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        LineChart chart = (LineChart) rootView.findViewById(R.id.chart);
        chart.setBackgroundColor(Color.WHITE);
        chart.animateXY(3000, 3000);
        setGraphValues(chart);


        //((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        iv_EditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentEditProfile = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intentEditProfile);
            }
        });

        return rootView;
    }


    @OnClick(R.id.iv_EditProfile)
    public void editProfile() {
        Toast.makeText(getActivity(), "Hello", Toast.LENGTH_LONG).show();
        Intent intentEditProfile = new Intent(getActivity(), EditProfileActivity.class);
        startActivity(intentEditProfile);
    }


    @OnClick(R.id.buttonCertifications)
    public void certifications() {
        Intent intentEditProfile = new Intent(getActivity(), CertificationsActivity.class);
        startActivity(intentEditProfile);
    }

    @OnClick(R.id.civ_profile_image)
    public void changeImage() {
        Intent intentEditProfile = new Intent(getActivity(), EditProfileActivity.class);
        startActivity(intentEditProfile);
    }


    @OnClick(R.id.buttonPreSetRoutine)
    public void routines() {
        Intent intentEditProfile = new Intent(getActivity(), RoutinesActivity.class);
        startActivity(intentEditProfile);
    }


    @OnClick(R.id.buttonSpecialization)
    public void setButtonSpecialization() {
       /* Intent intentEditProfile = new Intent(getActivity(), TraineesActivity.class);
        startActivity(intentEditProfile);*/
    }

    private void setGraphValues(LineChart chart) {

        List<DemoGraphModal> list = new ArrayList<>();
        List<Entry> entries = new ArrayList<Entry>();

        for (int i = 0; i < 20; i++) {
            DemoGraphModal demoGraphModal = new DemoGraphModal();
            demoGraphModal.setX(i + 1);
            demoGraphModal.setY(i - 2);
            list.add(demoGraphModal);
        }


        for (DemoGraphModal demoGraphModal : list) {

            // turn your data into Entry objects
            entries.add(new Entry(demoGraphModal.getX(), demoGraphModal.getY()));
        }


        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        dataSet.setColor(R.color.colorPrimary);
        dataSet.setValueTextColor(R.color.fb_grey); // styling, ...
        dataSet.setHighlightEnabled(true);
        dataSet.setDrawHighlightIndicators(true);
        dataSet.setDrawFilled(true);
        dataSet.setFillDrawable(getResources().getDrawable(R.drawable.round_button_drawable));
        //dataSet.setHighLightColor(Color.BLACK);
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.invalidate(); // refresh

    }
}
