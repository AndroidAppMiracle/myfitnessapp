package com.myfitnessapp.activity.trainee_module.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.myfitnessapp.R;
import com.myfitnessapp.adapter.SearchTrainerAdapter;
import com.myfitnessapp.helper.ItemClickSupport;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 8/11/2017.
 */

public class SearchTrainersActivity extends MyFitnessBaseActivity {


    @BindView(R.id.aiv_back)
    ImageView aiv_back;

    @BindView(R.id.aet_search)
    AppCompatEditText aet_search;

    @BindView(R.id.aiv_clear)
    AppCompatImageView aiv_clear;

    @BindView(R.id.atv_message)
    AppCompatTextView atv_message;

    @BindView(R.id.rv_search_list)
    RecyclerView rv_search_list;

    @BindView(R.id.fab_add_trainer)
    FloatingActionButton fab_add_trainer;

    List<DemoDataModal> list = new ArrayList<>();

    SearchTrainerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_trainers);
        ButterKnife.bind(this);

        rv_search_list.setLayoutManager(new LinearLayoutManager(SearchTrainersActivity.this));
        rv_search_list.setItemAnimator(new DefaultItemAnimator());
        rv_search_list.addItemDecoration(new DividerItemDecoration(SearchTrainersActivity.this, LinearLayoutManager.VERTICAL));

        addItemsToList();


        aet_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                if (!aet_search.getText().toString().equalsIgnoreCase("")) {
                    String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(queryText);
                }

            }
        });


        aet_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (!aet_search.getText().toString().equalsIgnoreCase("")) {
                    String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
                    adapter.filter(queryText);
                    return true;
                } else {
                    return false;
                }

            }
        });


        ItemClickSupport.addTo(rv_search_list).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Intent intentTrainerProfile = new Intent(SearchTrainersActivity.this, ViewTrainerProfileActivity.class);
                startActivity(intentTrainerProfile);
            }
        });


    }

    @OnClick(R.id.aiv_back)
    public void onClickBack() {
        SearchTrainersActivity.this.finish();
    }

    @OnClick(R.id.aiv_clear)
    public void OnClickClearSearchQuery() {

        if (!aet_search.getText().toString().equalsIgnoreCase("")) {
            aet_search.setText("");
            String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
            adapter.filter(queryText);
        }

    }


    @OnClick(R.id.fab_add_trainer)
    public void onClickAddTrainer() {
        Intent intentAddTrainer = new Intent(SearchTrainersActivity.this, AddNewTrainer.class);
        startActivity(intentAddTrainer);

    }


    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("John " + (i + 1));
            demoDataModal.setSongUrl("Description about trainer.");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }

        adapter = new SearchTrainerAdapter(SearchTrainersActivity.this, list);
        rv_search_list.setAdapter(adapter);
    }
}
