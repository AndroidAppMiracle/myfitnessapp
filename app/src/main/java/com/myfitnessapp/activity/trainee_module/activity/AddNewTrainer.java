package com.myfitnessapp.activity.trainee_module.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.myfitnessapp.R;
import com.myfitnessapp.activity.trainer_module.EditPresetRoutineActivity;
import com.myfitnessapp.helper.MyFitnessBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/11/2017.
 */

public class AddNewTrainer extends MyFitnessBaseActivity {

    @BindView(R.id.etTrainerName)
    AppCompatEditText etTrainerName;

    @BindView(R.id.etTrainerEmailId)
    AppCompatEditText etTrainerEmailId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.buttonAddTrainer)
    Button buttonAddTrainer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trainer);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Add Trainer");
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewTrainer.this.finish();
            }
        });
    }
}
