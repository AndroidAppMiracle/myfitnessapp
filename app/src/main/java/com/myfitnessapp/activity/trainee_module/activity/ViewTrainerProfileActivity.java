package com.myfitnessapp.activity.trainee_module.activity;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.myfitnessapp.R;
import com.myfitnessapp.activity.trainer_module.TrainerReviewsActivity;
import com.myfitnessapp.adapter.TrainerReviewsAdapter;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by satoti.garg on 8/10/2017.
 */

public class ViewTrainerProfileActivity extends MyFitnessBaseActivity {


    @BindView(R.id.main_toolbar)
    Toolbar main_toolbar;

    @BindView(R.id.civ_profile_image)
    CircleImageView civ_profile_image;

    @BindView(R.id.buttonStartWorking)
    Button buttonStartWorking;

    @BindView(R.id.atvViewAll)
    AppCompatTextView atvViewAll;

    @BindView(R.id.rv_Reviews)
    RecyclerView rv_Reviews;
    List<DemoDataModal> list = new ArrayList<>();
    TrainerReviewsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trainer_profile);
        ButterKnife.bind(this);

        setSupportActionBar(main_toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        rv_Reviews.setLayoutManager(new LinearLayoutManager(ViewTrainerProfileActivity.this));
        rv_Reviews.setItemAnimator(new DefaultItemAnimator());
        rv_Reviews.addItemDecoration(new DividerItemDecoration(ViewTrainerProfileActivity.this, LinearLayoutManager.VERTICAL));
        rv_Reviews.setNestedScrollingEnabled(false);

       /* toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewTrainerProfileActivity.this.finish();
            }
        });*/

        addItemsToList();

    }



    @OnClick(R.id.buttonStartWorking)
    public void startWorkingWithTrainer()
    {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(ViewTrainerProfileActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(ViewTrainerProfileActivity.this);
        }

        builder.setTitle("Start Working")
                .setMessage("Are you sure you want to start working with the trainer?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        Toast.makeText(ViewTrainerProfileActivity.this,"Yes is clicked",Toast.LENGTH_LONG).show();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        Toast.makeText(ViewTrainerProfileActivity.this,"No is clicked",Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 3; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Trainee " + (i + 1));
            demoDataModal.setSongUrl((i + 1) + " Months ago");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }

        adapter = new TrainerReviewsAdapter(ViewTrainerProfileActivity.this, list);
        rv_Reviews.setAdapter(adapter);
    }
}
