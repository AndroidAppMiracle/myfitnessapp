package com.myfitnessapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.myfitnessapp.MainActivity;
import com.myfitnessapp.TrainerSignUpActivity;
import com.myfitnessapp.custome_controls.CustomeTextViewRegular;
import com.myfitnessapp.helper.Constants;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.R;
import com.myfitnessapp.helper.NetworkConnection;
import com.myfitnessapp.helper.Utility;
import com.myfitnessapp.modal.RegisterModal;
import com.myfitnessapp.modal.RegisterModalData;
import com.myfitnessapp.modal.TrainerRegistrationModel;
import com.myfitnessapp.seversNetworkManagers.APIServerResponse;
import com.myfitnessapp.seversNetworkManagers.ServerAPI;
import com.myfitnessapp.util.Config;
import com.myfitnessapp.util.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/4/2017.
 */

public class LoginActivity extends MyFitnessBaseActivity implements APIServerResponse {


    @BindView(R.id.etEmailID)
    EditText etEmailID;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    @BindView(R.id.atvForgotPassword)
    CustomeTextViewRegular atvForgotPassword;

    @BindView(R.id.buttonFBLogin)
    Button buttonFBLogin;

    @BindView(R.id.buttonGoogleLogin)
    Button buttonGoogleLogin;

    @BindView(R.id.atvSignUp)
    LinearLayout atvSignUp;
    CallbackManager callbackManager;
    private int RC_SIGN_IN = 2;
    private GoogleApiClient mGoogleApiClient;
    String typeuser = "trainee";
    String fbId = "", googleId = "", countryCode = "", phoneNumber = "", emailIdEt = "", gender = "MALE", photoUrl = "";
    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        ButterKnife.bind(this);
        if (getUserLoggedIn()) {
            if (getUserRole().equalsIgnoreCase("\"TRAINEE\"")) {
                Intent intentSignUp = new Intent(LoginActivity.this, MainActivity.class);
                intentSignUp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentSignUp);
                finish();
            } else if (getUserRole().equalsIgnoreCase("\"TRAINER\"")) {

            }
        }

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        setDrawableEdiTextImages();
        callbackManager = CallbackManager.Factory.create();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //  txtMessage.setText(message);
                }
            }
        };
        displayFirebaseRegId();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.myfitnessapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
        }


    }

    @OnClick(R.id.contine_txt)
    public void signTrainerSignup() {
        Intent intent = new Intent(getApplicationContext(), TrainerSignUpActivity.class);
        startActivity(intent);
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);
        setFcm(regId);
        if (!TextUtils.isEmpty(regId)) {
            //   Log.d("FCM Text", regId)
            Toast.makeText(this, "" + regId, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "FCM not received yet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // registerTrainee GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // registerTrainee new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    public void setDrawableEdiTextImages() {
        Drawable drawable = ContextCompat.getDrawable(LoginActivity.this, R.drawable.ic_action_lock);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5),
                (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd = new ScaleDrawable(drawable, 0, 50, 20);
        etPassword.setCompoundDrawables(null, null, sd.getDrawable(), null);

        Drawable drawable1 = ContextCompat.getDrawable(LoginActivity.this, R.drawable.ic_action_email);
        drawable1.setBounds(0, 0, (int) (drawable1.getIntrinsicWidth() * 0.5),
                (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd1 = new ScaleDrawable(drawable1, 0, 50, 20);
        etEmailID.setCompoundDrawables(null, null, sd1.getDrawable(), null);

    }


    @OnClick(R.id.atvSignUp)
    public void getSignUpScreen() {
        try {
            Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
            /*i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);*/
            startActivity(i);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.buttonLogin)
    public void login() {
        try {
            if (etEmailID.getText().toString().isEmpty()) {
                Toast.makeText(LoginActivity.this, "Please enter a valid Email-ID.", Toast.LENGTH_SHORT).show();
            } else if (etPassword.getText().toString().isEmpty()) {
                Toast.makeText(LoginActivity.this, "Please enter password.", Toast.LENGTH_SHORT).show();
            } else {
               /* Intent intentSignUp = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intentSignUp);*/
                loginApiCall();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loginApiCall() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().login(APIServerResponse.LOGIN, etEmailID.getText().toString(), etPassword.getText().toString(), "ANDROID", Constants.FCM_TOKEN, LoginActivity.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @OnClick(R.id.atvForgotPassword)
    public void forgotPassWord() {
        Intent i = new Intent(LoginActivity.this, ForgotPassword.class);
            /*i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);*/
        startActivity(i);
    }

    @OnClick(R.id.rl_google_login)
    public void googleLogin() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(LoginActivity.this);
        }
        builder.setTitle("Login AS")
                .setMessage(null)
                .setPositiveButton("Trainee", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        typeuser = "trainee";
                        googleLoginClick();
                        Toast.makeText(LoginActivity.this, "Yes is clicked", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Trainer", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        typeuser = "trainer";
                        googleLoginClick();
                        Toast.makeText(LoginActivity.this, "No is clicked", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public void googleLoginClick() {
        try {
            if (NetworkConnection.checkInternetConnection(LoginActivity.this)) {
                signIn();
            } else {
                Utility.showAlertDialog(LoginActivity.this);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @OnClick(R.id.rl_fb_login)
    public void facebookLoginLayoutClick() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(LoginActivity.this);
        }
        builder.setTitle("Login AS")
                .setMessage(null)
                .setPositiveButton("Trainee", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        typeuser = "trainee";
                        fbLoginClicked();
                        Toast.makeText(LoginActivity.this, "Trainee is clicked", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Trainer", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        typeuser = "trainer";
                        fbLoginClicked();
                        Toast.makeText(LoginActivity.this, "Trainer is clicked", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void fbLoginClicked() {
        try {
            if (NetworkConnection.checkInternetConnection(LoginActivity.this)) {
                // Session.setLoginTypeButton(pref, "facebook");
                loginFacebook();
            } else {
                Utility.showAlertDialog(LoginActivity.this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loginFacebook() {

        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                if (response != null) {
                                    try {
                                        String mFbid = object.getString("id");
                                        String name = object.getString("name");
                                        // String email = object.getString("email");
                                        //    registerSocialLogin(email);
                                        JSONObject jsonObject = object.getJSONObject("picture");
                                        JSONObject newJosn = jsonObject.getJSONObject("data");
                                        photoUrl = newJosn.getString("url");
                                        String fullName[] = name.split(" ");
                                        fbId = mFbid;
                                        String emailSt = "";


                                        if (object.has("email")) {
                                            emailSt = object.getString("email");
                                            showAlertForSocialLogin(fullName, emailSt, fbId, "FACEBOOK");
                                        } else {
                                            showAlertForSocialLogin(fullName, emailSt, fbId, "FACEBOOK");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                Log.d("----fb response----", "" + response);

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("onCancel", "Login");
                // Toast.makeText(Login.this,"on cancel",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("onError", "Login");
                // Toast.makeText(Login.this,"on Error",Toast.LENGTH_LONG).show();
            }
        });
    }


    public void showAlertForSocialLogin(final String fullName[], final String emailSt, final String socialId, final String socialType) {
        final String[] newEmail = {""};

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custome_social_layout, null);
        dialogBuilder.setView(dialogView);

        final EditText etEmailID = (EditText) dialogView.findViewById(R.id.etEmailID);
        if (emailSt.isEmpty() || emailSt.equals("")) {
            etEmailID.setVisibility(View.VISIBLE);
        } else {
            newEmail[0] = emailSt;
            etEmailID.setVisibility(View.GONE);
        }
        final EditText country_code_et = (EditText) dialogView.findViewById(R.id.country_code_et);
        final EditText etPhoneNumber = (EditText) dialogView.findViewById(R.id.etPhoneNumber);

        RadioGroup rd_grp_gender = (RadioGroup) dialogView.findViewById(R.id.rd_grp_gender);
        final RadioButton rdbtn_female = (RadioButton) dialogView.findViewById(R.id.rdbtn_female);
        final RadioButton rdbtn_male = (RadioButton) dialogView.findViewById(R.id.rdbtn_male);
        dialogBuilder.setTitle("Enter detail..");
        dialogBuilder.setMessage(null);

        rd_grp_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbtn_female:

                        gender = rdbtn_female.getText().toString();
                        Log.e("Id", "Id:=" + rdbtn_female.getId());
                        break;
                    case R.id.rdbtn_male:

                        gender = rdbtn_male.getText().toString();
                        Log.e("Id", "Id:=" + rdbtn_male.getId());
                        break;


                }
            }
        });

        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                if (emailSt.equals("") || emailSt.isEmpty()) {
                    newEmail[0] = etEmailID.getText().toString();
                } else {

                }
                if (fullName.length == 2) {
                    socialtraineeLogin(fullName[0], fullName[1], newEmail[0], "", country_code_et.getText().toString(), etPhoneNumber.getText().toString(), socialId, socialType);
                } else {
                    socialtraineeLogin(fullName[0], "", newEmail[0], "", country_code_et.getText().toString(), etPhoneNumber.getText().toString(), socialId, socialType);
                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    private void signIn() {

        try {
            loginWithGoogle();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loginWithGoogle() {
        try {
            // Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                /*.enableAutoManage(Login.this *//* FragmentActivity *//*, Login.this *//* OnConnectionFailedListener *//*)*/
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            //mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    //https://developers.google.com/mobile/add?refresh=1
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (!acct.getDisplayName().isEmpty()) {
                String name = acct.getDisplayName();
                // Session.setGooglName(pref, name);
            }

            if (acct.getId().isEmpty() || acct.getId().equals(null)) {
                googleId = acct.getId();
            }
            String email = acct.getEmail();
            if (acct.getPhotoUrl() != null) {
                photoUrl = acct.getPhotoUrl().toString();
                Log.i("PhotoUri", "" + photoUrl);
                //   Session.set_google_signin_user_profile_pic(pref, photoUrl);
            }
            GoogleSignInAccount acct1 = result.getSignInAccount();
            String name = acct.getDisplayName();

            final String fullName[] = name.split("");
            String email1 = acct.getEmail();

            if (acct.getPhotoUrl() != null) {
                String photoUrl = acct.getPhotoUrl().toString();
                Log.i("PhotoUri", "" + photoUrl);
                //Session.set_google_signin_user_profile_pic(pref, photoUrl);
            }

            showAlertForSocialLogin(fullName, email1, googleId, "GOOGLE");
        }
    }


    public void socialtraineeLogin(String firstName, String lastName, String emailSt, String password, String countryCode, String phoneNumber, String socialId, String socialMode) {
        if (isConnectedToInternet()) {
            showLoading();
            if (typeuser.equals("trainee")) {
                ServerAPI.getInstance().traineeSocialLogin(APIServerResponse.REGISTER, firstName, lastName,
                        emailSt, password, countryCode,
                        socialId, socialMode, Constants.FCM_TOKEN, "ANDROID", gender, "",
                        phoneNumber, "1", "", "",
                        "", "", "", photoUrl, LoginActivity.this);
            } else if (typeuser.equals("trainer")) {
                ServerAPI.getInstance().trainerSocialLogin(APIServerResponse.TRAINEE_REGISTER, firstName, lastName,
                        emailSt, password, countryCode,
                        socialId, socialMode, Constants.FCM_TOKEN, "ANDROID", gender, "",
                        phoneNumber, "1", "", "",
                        "", photoUrl, LoginActivity.this);
            }

        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.LOGIN:
                        JsonObject registerModal = (JsonObject) response.body();

                        if (registerModal.get("statusCode").toString().equalsIgnoreCase("200") || registerModal.get("statusCode").toString().equalsIgnoreCase("201")) {
                            showToast("" + registerModal.get("message"), Toast.LENGTH_SHORT);
                            JsonObject jsonObject1 = registerModal.getAsJsonObject("data");
                            setUserID(jsonObject1.get("_id").toString());
                            setEmailID(jsonObject1.get("email").toString());
                            setAccessToken(jsonObject1.get("accessToken")+"");
                            setPhoneNumber(jsonObject1.get("phoneNo").toString());
                            setUserRole(jsonObject1.get("role").toString());
                            setUserLoggedIn(true);
                            if (jsonObject1.get("role").toString().equalsIgnoreCase("\"TRAINEE\"")) {
                                //Toast.makeText(getApplicationContext(), "Trainer Role", Toast.LENGTH_SHORT).show();
                                Intent intentSignUp = new Intent(LoginActivity.this, MainActivity.class);
                                intentSignUp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intentSignUp);
                                finish();
                            } else {

                            }

                        } else {
                            showToast("" + registerModal.get("message"), Toast.LENGTH_SHORT);
                        }
                        break;

                    case APIServerResponse.REGISTER:
                        RegisterModal registerModal1 = (RegisterModal) response.body();
                        if (registerModal1.getStatusCode().equalsIgnoreCase("200") || registerModal1.getStatusCode().equalsIgnoreCase("201")) {
                            showToast("" + registerModal1.getMessage(), Toast.LENGTH_SHORT);
                            Intent intentSignUp = new Intent(LoginActivity.this, MainActivity.class);
                            intentSignUp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentSignUp);
                            finish();
                        } else {
                            showToast("" + registerModal1.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                    case APIServerResponse.TRAINEE_REGISTER:
                        TrainerRegistrationModel trainerRegistrationModel = (TrainerRegistrationModel) response.body();
                        if (trainerRegistrationModel.getStatusCode().equalsIgnoreCase("200") || trainerRegistrationModel.getStatusCode().equalsIgnoreCase("201")) {
                            showToast("" + trainerRegistrationModel.getMessage(), Toast.LENGTH_SHORT);
                            /* Intent intentSignUp = new Intent(TrainerSignUpActivity.this, MainActivity.class);
                            intentSignUp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentSignUp);
                            finish();*/
                        } else {
                            showToast("" + trainerRegistrationModel.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            hideLoading();
            showToast("unsuccessfull", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        if (throwable.getMessage().equalsIgnoreCase(": open failed: ENOENT (No such file or directory)")) {
            showToast("Please upload image..", Toast.LENGTH_SHORT);
        }
    }
}
