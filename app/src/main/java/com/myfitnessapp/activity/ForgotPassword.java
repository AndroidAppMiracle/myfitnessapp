package com.myfitnessapp.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.login.Login;
import com.myfitnessapp.custome_controls.CustomeEditTextRegular;
import com.myfitnessapp.helper.Constants;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.R;
import com.myfitnessapp.modal.StatusMessageDataResponse;
import com.myfitnessapp.seversNetworkManagers.APIServerResponse;
import com.myfitnessapp.seversNetworkManagers.ServerAPI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class ForgotPassword extends MyFitnessBaseActivity implements APIServerResponse {


    @BindView(R.id.etEmailID)
    CustomeEditTextRegular etEmailID;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForgotPassword.this.finish();
            }
        });

        Drawable drawable = ContextCompat.getDrawable(ForgotPassword.this, R.drawable.ic_action_email);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5),
                (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd1 = new ScaleDrawable(drawable, 0, 50, 20);
        etEmailID.setCompoundDrawables(null, null, sd1.getDrawable(), null);
    }


    @OnClick(R.id.buttonLogin)
    public void fogetPassword() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().resetPassword(APIServerResponse.FORGET_PASSWORD, etEmailID.getText().toString(), ForgotPassword.this);
        } else {
            showSnack(Constants.INTERNET_CONNECTION);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.FORGET_PASSWORD:
                        StatusMessageDataResponse statusMessageDataResponse = (StatusMessageDataResponse) response.body();
                        if (statusMessageDataResponse.getStatusCode().equalsIgnoreCase("200") || statusMessageDataResponse.getStatusCode().equalsIgnoreCase("201")) {
                            showToast(statusMessageDataResponse.getMessage(), Toast.LENGTH_SHORT);
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                        } else {
                            showToast(statusMessageDataResponse.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
