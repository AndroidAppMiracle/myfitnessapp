package com.myfitnessapp.activity.trainer_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.myfitnessapp.R;
import com.myfitnessapp.helper.MyFitnessBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/8/2017.
 */

public class UpdateCertificationsActivity extends MyFitnessBaseActivity {


    @BindView(R.id.etEditCertificate)
    AppCompatEditText etEditCertificate;

    @BindView(R.id.buttonSignUp)
    Button buttonSignUp;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_certification);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Update Certification");
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateCertificationsActivity.this.finish();
            }
        });

    }
}
