package com.myfitnessapp.activity.trainer_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.myfitnessapp.R;
import com.myfitnessapp.helper.MyFitnessBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class EditPresetRoutineActivity extends MyFitnessBaseActivity {

    @BindView(R.id.etRoutineName)
    AppCompatEditText etRoutineName;

    @BindView(R.id.etRoutineComments)
    AppCompatEditText etRoutineComments;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.buttonSaveRoutine)
    Button buttonSaveRoutine;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_preset_routine);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Edit Routine");
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditPresetRoutineActivity.this.finish();
            }
        });
    }
}
