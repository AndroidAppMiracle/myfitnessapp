package com.myfitnessapp.activity.trainer_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.myfitnessapp.R;
import com.myfitnessapp.adapter.TraineesAdapter;
import com.myfitnessapp.helper.ItemClickSupport;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/8/2017.
 */

public class TraineesActivity extends MyFitnessBaseActivity {


    @BindView(R.id.rv_Trainees)
    RecyclerView rv_Trainees;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<DemoDataModal> list = new ArrayList<>();


    TraineesAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainees);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Trainees");
        }
        rv_Trainees.setLayoutManager(new LinearLayoutManager(TraineesActivity.this));
        rv_Trainees.setItemAnimator(new DefaultItemAnimator());
        rv_Trainees.addItemDecoration(new DividerItemDecoration(TraineesActivity.this, LinearLayoutManager.VERTICAL));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TraineesActivity.this.finish();
            }
        });

        addItemsToList();


        ItemClickSupport.addTo(rv_Trainees).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

            }
        });

    }

    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("John " + (i + 1));
            demoDataModal.setSongUrl("Description about trainee.");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }

        adapter = new TraineesAdapter(TraineesActivity.this, list);
        rv_Trainees.setAdapter(adapter);
    }
}
