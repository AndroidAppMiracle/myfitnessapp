package com.myfitnessapp.activity.trainer_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.myfitnessapp.R;
import com.myfitnessapp.adapter.RoutinesAdapter;
import com.myfitnessapp.helper.ItemClickSupport;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/8/2017.
 */

public class RoutinesActivity extends MyFitnessBaseActivity {


    @BindView(R.id.rv_Routines)
    RecyclerView rv_Routines;

    @BindView(R.id.fab_add_certification)
    FloatingActionButton fab_add_certification;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<DemoDataModal> list = new ArrayList<>();

    RoutinesAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routines);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Routines");
        }


        rv_Routines.setLayoutManager(new LinearLayoutManager(RoutinesActivity.this));
        rv_Routines.setItemAnimator(new DefaultItemAnimator());
        rv_Routines.addItemDecoration(new DividerItemDecoration(RoutinesActivity.this, LinearLayoutManager.VERTICAL));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RoutinesActivity.this.finish();
            }
        });

        addItemsToList();


        ItemClickSupport.addTo(rv_Routines).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

            }
        });
    }

    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Fitness Routine " + (i + 1));
            demoDataModal.setSongUrl("Description of a fitness routine.");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }

        adapter = new RoutinesAdapter(RoutinesActivity.this, list);
        rv_Routines.setAdapter(adapter);
    }
}
