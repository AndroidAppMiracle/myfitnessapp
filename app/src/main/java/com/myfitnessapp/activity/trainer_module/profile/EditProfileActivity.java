package com.myfitnessapp.activity.trainer_module.profile;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.myfitnessapp.R;
import com.myfitnessapp.helper.MyFitnessBaseActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by satoti.garg on 8/8/2017.
 */

public class EditProfileActivity extends MyFitnessBaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.etDOB)
    AppCompatEditText etDOB;

    @BindView(R.id.civ_profile_image)
    CircleImageView civ_profile_image;

    @BindView(R.id.rl_profile_image)
    RelativeLayout rl_profile_image;

    @BindView(R.id.etFirstName)
    AppCompatEditText etFirstName;

    @BindView(R.id.etLastName)
    AppCompatEditText etLastName;

    @BindView(R.id.etEmailID)
    AppCompatEditText etEmailID;

    @BindView(R.id.etPassword)
    AppCompatEditText etPassword;

    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;

    @BindView(R.id.rd_grp_gender)
    RadioGroup rd_grp_gender;

    @BindView(R.id.buttonSignUp)
    Button buttonSignUp;


    //Views Only For Trainee
    @BindView(R.id.etFat)
    AppCompatEditText etFat;

    @BindView(R.id.etWeight)
    AppCompatEditText etWeight;

    @BindView(R.id.etRecentGym)
    AppCompatEditText etRecentGym;

    @BindView(R.id.etCity)
    AppCompatEditText etCity;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Edit Profile");
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileActivity.this.finish();
            }
        });


    }


    @OnClick(R.id.etDOB)
    public void getDoB() {

        openDateDia();
    }


    /*private void setGraphValues(LineChart chart) {

        List<DemoGraphModal> list = new ArrayList<>();
        List<Entry> entries = new ArrayList<Entry>();

        for (int i = 0; i < 20; i++) {
            DemoGraphModal demoGraphModal = new DemoGraphModal();
            demoGraphModal.setX(i + 1);
            demoGraphModal.setY(i - 2);
            list.add(demoGraphModal);
        }


        for (DemoGraphModal demoGraphModal : list) {

            // turn your data into Entry objects
            entries.add(new Entry(demoGraphModal.getX(), demoGraphModal.getY()));
        }


        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        dataSet.setColor(R.color.colorPrimary);
        dataSet.setValueTextColor(R.color.fb_grey); // styling, ...
        dataSet.setHighlightEnabled(true);
        dataSet.setDrawHighlightIndicators(true);
        dataSet.setDrawFilled(true);
        *//*dataSet.setFillDrawable(getResources().getDrawable(R.drawable.round_button_drawable));*//*
        //dataSet.setHighLightColor(Color.BLACK);
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.invalidate(); // refresh

    }*/


    Calendar calendar = Calendar.getInstance();

    public void openDateDia() {
        final Calendar cal = Calendar.getInstance();

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime());
                        Log.e("Date is", "Date is" + format);
                        /*yyyy-MM-dd*/


                        etDOB.setText(format);
                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);

        datePicker.setCancelable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    /*Check valid date of birth is valid*/
    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse(pDateString);
        /*Date date = new SimpleDateFormat("dd-MM-yyyy").parse(pDateString);*/
        /*Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pDateString);*/
        return new Date().before(date);
    }




    public void editProfile()
    {

    }

}
