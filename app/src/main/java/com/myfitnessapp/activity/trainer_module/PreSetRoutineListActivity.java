package com.myfitnessapp.activity.trainer_module;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.myfitnessapp.R;
import com.myfitnessapp.adapter.PreSetRoutineListAdapter;
import com.myfitnessapp.helper.ItemClickSupport;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class PreSetRoutineListActivity extends MyFitnessBaseActivity {


    @BindView(R.id.rv_PresetRoutine)
    RecyclerView rv_PresetRoutine;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<DemoDataModal> list = new ArrayList<>();
    PreSetRoutineListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preset_routinelist);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Routine List");
        }


        rv_PresetRoutine.setLayoutManager(new LinearLayoutManager(PreSetRoutineListActivity.this));
        rv_PresetRoutine.setItemAnimator(new DefaultItemAnimator());
        rv_PresetRoutine.addItemDecoration(new DividerItemDecoration(PreSetRoutineListActivity.this, LinearLayoutManager.VERTICAL));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreSetRoutineListActivity.this.finish();
            }
        });

        addItemsToList();

        ItemClickSupport.addTo(rv_PresetRoutine).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intentEdit = new Intent(PreSetRoutineListActivity.this, EditPresetRoutineActivity.class);
                startActivity(intentEdit);
            }
        });


    }

    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Fitness Routine " + (i + 1));
            demoDataModal.setSongUrl((i + 1) + " Months ago");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }

        adapter = new PreSetRoutineListAdapter(PreSetRoutineListActivity.this, list);
        rv_PresetRoutine.setAdapter(adapter);
    }
}
