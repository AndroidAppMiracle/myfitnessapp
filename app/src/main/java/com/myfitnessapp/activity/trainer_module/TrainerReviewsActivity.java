package com.myfitnessapp.activity.trainer_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.R;
import com.myfitnessapp.adapter.TrainerReviewsAdapter;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/7/2017.
 */

public class TrainerReviewsActivity extends MyFitnessBaseActivity {

    @BindView(R.id.rv_TrainerReviews)
    RecyclerView rv_TrainerReviews;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    List<DemoDataModal> list = new ArrayList<>();
    TrainerReviewsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer_reviews);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Reviews");
        }


        rv_TrainerReviews.setLayoutManager(new LinearLayoutManager(TrainerReviewsActivity.this));
        rv_TrainerReviews.setItemAnimator(new DefaultItemAnimator());
        rv_TrainerReviews.addItemDecoration(new DividerItemDecoration(TrainerReviewsActivity.this, LinearLayoutManager.VERTICAL));
        rv_TrainerReviews.setHasFixedSize(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TrainerReviewsActivity.this.finish();
            }
        });

        addItemsToList();


    }

    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Trainee " + (i + 1));
            demoDataModal.setSongUrl((i + 1) + " Months ago");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }

        adapter = new TrainerReviewsAdapter(TrainerReviewsActivity.this, list);
        rv_TrainerReviews.setAdapter(adapter);
    }
}
