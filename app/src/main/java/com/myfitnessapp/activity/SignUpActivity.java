package com.myfitnessapp.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.myfitnessapp.MainActivity;
import com.myfitnessapp.helper.Constants;
import com.myfitnessapp.helper.MyFitnessBaseActivity;
import com.myfitnessapp.R;
import com.myfitnessapp.modal.RegisterModal;
import com.myfitnessapp.seversNetworkManagers.APIServerResponse;
import com.myfitnessapp.seversNetworkManagers.ServerAPI;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/4/2017.
 */

public class SignUpActivity extends MyFitnessBaseActivity implements EasyPermissions.PermissionCallbacks, ImagePickerCallback, APIServerResponse {


    @BindView(R.id.rl_profile_image)
    RelativeLayout rl_profile_image;

    @BindView(R.id.civ_profile_image)
    CircleImageView civ_profile_image;

    @BindView(R.id.civ_take_image)
    CircleImageView civ_take_image;

    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etLastName)
    EditText etLastName;

    @BindView(R.id.etEmailID)
    EditText etEmailID;
    //country_code_et
    @BindView(R.id.country_code_et)
    EditText etCountryCode;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etDOB)
    EditText etDOB;
    @BindView(R.id.spSpecialization)
    Spinner spSpecialization;
    @BindView(R.id.etCertifications)
    EditText etCertifications;
    @BindView(R.id.etAboutMe)
    EditText etAboutMe;
    @BindView(R.id.etFat)
    EditText etFat;
    @BindView(R.id.etWeight)
    EditText etWeight;
    @BindView(R.id.etRecentGym)
    EditText etRecentGym;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etTrainer)
    EditText etTrainer;
    //etTrainer
    @BindView(R.id.buttonSignUp)
    Button buttonSignUp;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rd_grp_gender)
    RadioGroup rd_grp_gender;
    @BindView(R.id.rdbtn_female)
    RadioButton rdbtn_female;
    @BindView(R.id.rdbtn_male)
    RadioButton rdbtn_male;
    private String gender="MALE";
    String device_id;

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    private static final int PLACE_PICKER_REQUEST = 1;
    //String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET};
    //private int RC_LOCATION_INTERNET = 102;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "MyFitnessApp";
    private static final int RC_CAMERA_PERM = 342;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpActivity.this.finish();
            }
        });

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.specialization_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spSpecialization.setAdapter(adapter);
        spSpecialization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(SignUpActivity.this, adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        rd_grp_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbtn_female:

                        gender = rdbtn_female.getText().toString();
                        Log.e("Id", "Id:=" + rdbtn_female.getId());

                        break;
                    case R.id.rdbtn_male:

                        gender = rdbtn_male.getText().toString();
                        Log.e("Id", "Id:=" + rdbtn_male.getId());

                        break;


                }
            }
        });
    }

    @OnClick(R.id.civ_take_image)
    public void pickImage() {
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }

    public boolean validation() {

        if (etFirstName.getText().toString().isEmpty()) {
            showToast(Constants.FIRST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(etFirstName.getText().charAt(0))) {
            showToast(Constants.FIRST_NAME_START, Toast.LENGTH_SHORT);
            return false;
        } else if (etLastName.getText().toString().isEmpty()) {
            showToast(Constants.LAST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(etLastName.getText().charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (etPhoneNumber.getText().toString().isEmpty()) {
            showToast(Constants.PHONE_NUMBER_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (etFat.getText().toString().isEmpty() || etFat.getText().toString().equalsIgnoreCase(null)) {
            if (Integer.parseInt(etFat.getText().toString()) > 100) {
                showToast(Constants.FAT_PERCENTAGE, Toast.LENGTH_SHORT);
                return false;
            } else {
                showToast(Constants.FAT_PERCENTAGE, Toast.LENGTH_SHORT);
                return false;
            }
        } else if (etCountryCode.getText().toString().isEmpty()) {
            showToast(Constants.COUNTRY_CODE_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (etCountryCode.getText().toString().length() < 2) {
            showToast(Constants.COUNTRY_CODE_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            showToast(Constants.PHONE_NUMBER_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (etEmailID.getText().toString().isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(etEmailID.getText().charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (!isValidEmail(etEmailID.getText().toString())) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (etPassword.getText().toString().isEmpty()) {
            showToast(Constants.PASSWORD_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (etPassword.getText().toString().length() < 6) {
            showToast(Constants.PASSWORD_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (gender == "" || gender == null) {
            showToast("Select Gender", Toast.LENGTH_SHORT);
            return false;
        } else if (etDOB.getText().toString().isEmpty()) {
            showToast("Enter Date of Birth", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.buttonSignUp)
    public void saveInfo() {

        if (validation()) {
            if (EasyPermissions.hasPermissions(this, Manifest.permission.INTERNET)) {
                if (isConnectedToInternet()) {
                    showLoading();
                    ServerAPI.getInstance().registerTrainee(APIServerResponse.REGISTER, etFirstName.getText().toString().trim(), etLastName.getText().toString(),
                            etEmailID.getText().toString(), etPassword.getText().toString(), etCountryCode.getText().toString(),
                            "", "", Constants.FCM_TOKEN, "ANDROID", gender, etDOB.getText().toString(),
                            etPhoneNumber.getText().toString(), "1", etFat.getText().toString(), etWeight.getText().toString(),
                            etRecentGym.getText().toString(), etCity.getText().toString(), etTrainer.getText().toString(), pickerPath, SignUpActivity.this);
                } else {
                    showSnack(Constants.INTERNET_CONNECTION);
                }
            }
        }
    }

    int year, month, day;
    Calendar mcurrentDate = Calendar.getInstance();

    @OnClick(R.id.etDOB)
    public void setDateOfBirth() {
        year = mcurrentDate.get(Calendar.YEAR);
        month = mcurrentDate.get(Calendar.MONTH);
        day = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(SignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {

                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH, selectedday);
                String format = new SimpleDateFormat("yyyy-MM-dd").format(mcurrentDate.getTime());
                try {
                    //  if (isValidDate(format)) {


                    etDOB.setText(format);
                  /*  } else {
                        // showToast("Select Valid Date of Birth", Toast.LENGTH_SHORT);
                        etDOB.requestFocus();
                    }*/
                } catch (Exception ec) {
                    ec.printStackTrace();
                }
            }
        }, year, month, day);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    Calendar calendar = Calendar.getInstance();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void openDateDia() {
        hideKeyboard();
        final Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        try {
                            if (isValidDate(format)) {
                                etDOB.setText(format);
                            } else {
                                // showToast("Select Valid Date of Birth", Toast.LENGTH_SHORT);
                                etDOB.requestFocus();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        etDOB.setText(format);


                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());

        datePicker.setCancelable(true);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    /*Check valid date of birth is valid*/
    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pDateString);
        return new Date().before(date);
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            civ_profile_image.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.REGISTER:
                        RegisterModal registerModal = (RegisterModal) response.body();
                        if (registerModal.getStatusCode().equalsIgnoreCase("200") || registerModal.getStatusCode().equalsIgnoreCase("201")) {
                            showToast("" + registerModal.getMessage(), Toast.LENGTH_SHORT);
                            Intent intentSignUp = new Intent(SignUpActivity.this, MainActivity.class);
                            intentSignUp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentSignUp);
                            finish();
                        } else {
                            showToast("" + registerModal.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            showToast("unsuccessfull", Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }
}
