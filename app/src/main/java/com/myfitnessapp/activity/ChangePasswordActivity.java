package com.myfitnessapp.activity;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.myfitnessapp.R;
import com.myfitnessapp.custome_controls.CustomeEditTextRegular;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends AppCompatActivity {


    @BindView(R.id.etEmailID)
    CustomeEditTextRegular etEmailID;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangePasswordActivity.this.finish();
            }
        });

        Drawable drawable = ContextCompat.getDrawable(ChangePasswordActivity.this, R.drawable.ic_action_email);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5),
                (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd1 = new ScaleDrawable(drawable, 0, 50, 20);
        etEmailID.setCompoundDrawables(null, null, sd1.getDrawable(), null);
    }
}
