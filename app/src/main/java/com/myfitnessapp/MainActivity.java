package com.myfitnessapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.myfitnessapp.activity.trainee_module.fragment.ProfileFragmentTrainee;
import com.myfitnessapp.fragments.HomeFragment;
import com.myfitnessapp.fragments.MessagesFragment;
import com.myfitnessapp.fragments.MoreFragment;
import com.myfitnessapp.fragments.NotificationsFragment;
import com.myfitnessapp.fragments.ProfileFragment;
import com.myfitnessapp.helper.BottomNavigationViewHelper;
import com.myfitnessapp.helper.MyFitnessBaseActivity;

public class MainActivity extends MyFitnessBaseActivity {

    private FrameLayout content;
    private String tag;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    HomeFragment homeFragment = new HomeFragment();
                    replaceFragments(homeFragment);
                    /*switchingFragment(homeFragment);*/

                    return true;
                case R.id.navigation_notifications:

                    NotificationsFragment notificationsFragment = new NotificationsFragment();
                    replaceFragments(notificationsFragment);
                    //switchingFragment(notificationsFragment);

                    return true;
                case R.id.navigation_profile:

                    ProfileFragmentTrainee profileFragment = new ProfileFragmentTrainee();
                    replaceFragments(profileFragment);
                    // switchingFragment(profileFragment);
                    return true;

                case R.id.navigation_messages:

                    MessagesFragment messagesFragment = new MessagesFragment();
                    replaceFragments(messagesFragment);
                    //switchingFragment(messagesFragment);
                    return true;

                case R.id.navigation_more:

                    MoreFragment moreFragment = new MoreFragment();
                    replaceFragments(moreFragment);
                    //switchingFragment(moreFragment);
                    return true;

                /*default:

                    ProfileFragment profileFragment1 = new ProfileFragment();

                    replaceFragments(profileFragment1);

                    return true;*/
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        replaceFragments(new HomeFragment());

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    private void replaceFragments(Fragment newFragment) {

        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.content, newFragment);

            /*transaction.addToBackStack(null);*/
            // Commit the transaction
            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


   /* private void switchFragment(Fragment mFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = mFragment;
            //fragment = new YourFragment();
            fragmentTransaction.add(R.id.container, fragment, tag);
        } else {
            fragmentTransaction.attach(fragment);
        }

        Fragment curFrag = getSupportFragmentManager().getPrimaryNavigationFragment();
        if (curFrag != null) {
            fragmentTransaction.detach(curFrag);
        }

        //fragmentTransaction.setPrimaryNavigationFragment(fragment);
        *//*fragmentTransaction.setReorderingAllowed(true);*//*
        fragmentTransaction.commitNowAllowingStateLoss();
    }*/


    private void switchingFragment(/*int index, */Fragment mFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);

        // if the fragment has not yet been added to the container, add it first
        if (fragment == null) {
            fragment = mFragment;
            fragmentTransaction.add(R.id.container, fragment, tag);
        }

        fragmentTransaction.hide(fragment);
        fragmentTransaction.show(fragment);
        fragmentTransaction.commit();
    }


}
