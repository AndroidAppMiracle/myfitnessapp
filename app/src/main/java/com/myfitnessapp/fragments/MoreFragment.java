package com.myfitnessapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.myfitnessapp.R;
import com.myfitnessapp.activity.LoginActivity;
import com.myfitnessapp.activity.SignUpActivity;
import com.myfitnessapp.activity.trainer_module.PreSetRoutineListActivity;
import com.myfitnessapp.activity.trainer_module.TraineesActivity;
import com.myfitnessapp.activity.trainer_module.TrainerReviewsActivity;
import com.myfitnessapp.custome_controls.CustomeTextViewRegular;
import com.myfitnessapp.helper.MyFitnessBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 8/4/2017.
 */

public class MoreFragment extends Fragment {

    @BindView(R.id.atvLogout)
    AppCompatTextView atvLogout;

    @BindView(R.id.atvReview)
    AppCompatTextView atvReview;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.atvPresetRoutine)
    AppCompatTextView atvPresetRoutine;


    @BindView(R.id.atv_Trainees)
    AppCompatTextView atv_Trainees;

    @BindView(R.id.title_tv)
    CustomeTextViewRegular titleTV;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("More");
        titleTV.setText("More Options");

        return rootView;
    }


    @OnClick(R.id.atvLogout)
    public void logout() {
        ((MyFitnessBaseActivity) getActivity()).showToast("successfully logged out!", Toast.LENGTH_SHORT);
        ((MyFitnessBaseActivity) getActivity()).clearPreferences();
        Intent i = new Intent(getActivity(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

    }

    @OnClick(R.id.atvReview)
    public void trainerReviews() {
        Intent i = new Intent(getActivity(), TrainerReviewsActivity.class);
        startActivity(i);

    }

    @OnClick(R.id.atvPresetRoutine)
    public void presetRoutine() {
        Intent i = new Intent(getActivity(), PreSetRoutineListActivity.class);
        startActivity(i);

    }


    @OnClick(R.id.atv_Trainees)
    public void trainees() {
        Intent i = new Intent(getActivity(), TraineesActivity.class);
        startActivity(i);

    }
}
