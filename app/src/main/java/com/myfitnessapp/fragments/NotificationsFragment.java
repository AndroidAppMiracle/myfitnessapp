package com.myfitnessapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myfitnessapp.R;
import com.myfitnessapp.adapter.NotificationsAdapter;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/4/2017.
 */

public class NotificationsFragment extends Fragment {

    List<DemoDataModal> list = new ArrayList<>();

    NotificationsAdapter adapter;

    @BindView(R.id.rv_notifications)
    RecyclerView rv_notifications;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Notifications");

        rv_notifications.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_notifications.setItemAnimator(new DefaultItemAnimator());
        rv_notifications.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        addItemsToList();
        return rootView;
    }

    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Trainee " + (i + 1) + " has completed his training.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.happybirthdayvinyl.co.uk/images/album-artwork/big/grace-jones-nightclubbing-deluxe-edition-disc1.jpg");
            list.add(demoDataModal);
        }

        adapter = new NotificationsAdapter(getActivity(), list);
        rv_notifications.setAdapter(adapter);
    }
}
