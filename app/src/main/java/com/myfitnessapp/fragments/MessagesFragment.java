package com.myfitnessapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myfitnessapp.R;
import com.myfitnessapp.adapter.MessagesAdapter;
import com.myfitnessapp.adapter.NotificationsAdapter;
import com.myfitnessapp.custome_controls.CustomeTextViewRegular;
import com.myfitnessapp.modal.DemoDataModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/4/2017.
 */

public class MessagesFragment extends Fragment {

    List<DemoDataModal> list = new ArrayList<>();

    MessagesAdapter adapter;

    @BindView(R.id.rv_messages)
    RecyclerView rv_messages;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.title_tv)
    CustomeTextViewRegular titleTV;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this, rootView);


        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        titleTV.setText("Messages");

        rv_messages.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_messages.setItemAnimator(new DefaultItemAnimator());
        rv_messages.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        addItemsToList();

        return rootView;
    }

    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("@Fitness " + (i + 1) + " has sent u a message.");
            demoDataModal.setSongUrl("@John");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.happybirthdayvinyl.co.uk/images/album-artwork/big/grace-jones-nightclubbing-deluxe-edition-disc1.jpg");
            list.add(demoDataModal);
        }

        adapter = new MessagesAdapter(getActivity(), list);
        rv_messages.setAdapter(adapter);
    }

}
