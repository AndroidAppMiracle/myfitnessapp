package com.myfitnessapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.myfitnessapp.R;
import com.myfitnessapp.activity.trainee_module.activity.SearchTrainersActivity;
import com.myfitnessapp.adapter.CategoryAdapter;
import com.myfitnessapp.custome_controls.CustomeTextViewRegular;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 8/4/2017.
 */

public class HomeFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /* @BindView(R.id.fab_search_trainer)
     FloatingActionButton fab_search_trainer;
 */
    @BindView(R.id.category_rv)
    RecyclerView categoryRv;
    CategoryAdapter adapter;

    @BindView(R.id.title_tv)
    CustomeTextViewRegular titleTV;
    @BindView(R.id.button_save_btn)
    Button saveBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        titleTV.setText("Home");

        categoryRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoryRv.setItemAnimator(new DefaultItemAnimator());
        categoryRv.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        categoryRv.setHasFixedSize(true);
        addItemsToList();

/*
        for (int row = 0; row < 1; row++) {
            RadioGroup ll = new RadioGroup(getActivity());
            ll.setOrientation(LinearLayout.VERTICAL);

            for (int i = 1; i <= 5; i++) {
                RadioButton rdbtn = new RadioButton(getActivity());
                rdbtn.setId((row * 2) + i);
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                params1.gravity = Gravity.RIGHT;
                //  android:layout_gravity="right"
                rdbtn.setText("Radio " + rdbtn.getId());
                ll.addView(rdbtn,params1);
            }
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            params.gravity=Gravity.RIGHT;
            ((ViewGroup) rootView.findViewById(R.id.radiogroup)).addView(ll,params);
    }*/

        return rootView;
    }


    @OnClick(R.id.button_save_btn)
    public void saveBtnClick() {
        Intent intent = new Intent(getActivity(), SearchTrainersActivity.class);
        getActivity().startActivity(intent);
    }

    @OnClick(R.id.fab_search_trainer)
    public void onClickSearchTrainer() {
        Intent intentSearchTrainer = new Intent(getActivity(), SearchTrainersActivity.class);
        startActivity(intentSearchTrainer);
    }


    public void addItemsToList() {
/*
        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Trainee " + (i + 1));
            demoDataModal.setSongUrl((i + 1) + " Months ago");
            demoDataModal.setAlbumArt("Highly Professional and instrumental in my strength training and cardio.");
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.viralbrindes.com.br/content/interfaces/cms/userfiles/produtos/icone-fitness-709.png");
            list.add(demoDataModal);
        }*/

        // adapter = new CategoryAdapter(TrainerReviewsActivity.this, list);
        adapter = new CategoryAdapter(getActivity());
        categoryRv.setAdapter(adapter);
    }
}
